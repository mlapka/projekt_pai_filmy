<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CommentController extends AbstractController
{
    /**
     * @Method("POST")
     * @Route("/comment", name="comment_add")
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {
        $videoRepository = $entityManager->getRepository(Video::class);
        $model = $videoRepository->find($request->get('videoId'));

        $comment = new Comment();
        $comment->setVideo($model);
        $comment->setCreated(new \DateTime('now'));
        $comment->setValue($request->get('value'));
        $comment->setUser($this->getUser());

        $entityManager->persist($comment);
        $entityManager->flush();

        $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $jsonObject = $serializer->serialize($comment, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/comment/remove/{id}/{idVideo}", name="remove_comment")
     */
    public function remove(int $id, int $idVideo, Request $request, EntityManagerInterface $entityManager) {
        $commentRepository = $entityManager->getRepository(Comment::class);
        $model = $commentRepository->find($id);

        $entityManager->remove($model);
        $entityManager->flush();

        return $this->redirectToRoute('video_store', ['id' => $idVideo]);
    }
}
