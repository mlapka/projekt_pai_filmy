<?php

namespace App\Controller;

use App\Entity\Subtitle;
use App\Entity\Video;
use App\Form\VideoType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class VideoController extends AbstractController
{
    /**
     * @Route("/", name="last_video")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $videoRepository = $em->getRepository(Video::class);
        $actives = $videoRepository->findBy(['active' => true], ['id' => 'DESC']);
        return $this->render('video/index.html.twig', [
            'vidoes' => $actives,
            'videoPath' => $this->getParameter('video_directory')
        ]);
    }


    /**
     * @Route("/video/{id}", name="video_store")
     */
    public function store(int $id, Request $request, EntityManagerInterface $em) {
        $videoRepository = $em->getRepository(Video::class);
        $model = $videoRepository->find($id);

        $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $jsonObject = $serializer->serialize($model->getSubtitles(), 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return $this->render('video/store.html.twig', [
            'video' => $model,
            'subtitles' => $jsonObject,
            'videoPath' => $this->getParameter('video_directory')
        ]);
    }

    /**
     * @Route("myVideos", name="my_movies")
     */
    public function myMovies(Request $request, EntityManagerInterface $em)
    {
        $videoRepository = $em->getRepository(Video::class);
        $myMovieList = $videoRepository->findBy(['User' => $this->getUser()]);

        return $this->render('video/my_movies.html.twig', [
            'vidoes' => $myMovieList,
        ]);
    }


    /**
     * @Route("myVideos/edit/{id}", name="edit_video")
     */
    public function edit(int $id, EntityManagerInterface $em)
    {
        $videoRepository = $em->getRepository(Video::class);
        $model = $videoRepository->find($id);

        return $this->render('video/edit.html.twig', [
            'model' => $model,
            'videoPath' => $this->getParameter('video_directory')
        ]);
    }


    /**
     * @Route("myVideos/delete/{id}", name="remove_video")
     */
    public function remove(int $id, EntityManagerInterface $em)
    {
        $videoRepository = $em->getRepository(Video::class);
        $model = $videoRepository->find($id);

        $em->remove($model);
        $em->flush();

        return $this->render('video/my_movies.html.twig');
    }

    /**
     * @Method("POST")
     * @Route("myVideos/{id}/addSubtitle", name="add_subtitle")
     */
    public function addSubtitle(int $id, Request $request, EntityManagerInterface $em) {
        $videoRepository = $em->getRepository(Video::class);
        $video = $videoRepository->find($id);

        $start = $request->get('start');
        $duration = $request->get('duration');
        $text = $request->get('text');

        $subtitle = new Subtitle();
        $subtitle->setStart($start);
        $subtitle->setDuration($duration);
        $subtitle->setText($text);
        $subtitle->setVideo($video);

        $em->persist($subtitle);
        $em->flush();

        $encoders = [new JsonEncoder()]; // If no need for XmlEncoder
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $jsonObject = $serializer->serialize($subtitle, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
    }


    /**
     * @Route("myVideos/{id}/removeSubtitle/{idSub}", name="remove_subtitle")
     */
    public function removeSubtitle(int $id, int $idSub, Request $request, EntityManagerInterface $em) {
        $subtitleRepository = $em->getRepository(Subtitle::class);
        $subtitle = $subtitleRepository->find($idSub);

        $em->remove($subtitle);
        $em->flush();

        return $this->redirectToRoute('edit_video', ['id' => $id]);
    }


    /**
     * @Route("/upload", name="upload")
     */
    public function uploadVideo(Request $request, EntityManagerInterface $em)
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $file */
            $file = $form['filePath']->getData();
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();


            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                    $this->getParameter('video_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }


            $video->setFilePath($fileName);
            $video->setUser($this->getUser());
            $video->setCreated(new \DateTime('now'));
            $video->setUpdated(new \DateTime('now'));

            $em->persist($video);
            $em->flush();


            return $this->redirect($this->generateUrl('my_movies'));

        }

        return $this->render('video/upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }


}
